$(function () {
  $('.js-sidebar-toggle').on('click', function () {
    $('.sidebar').toggleClass('sidebar__active')
    $('body').toggleClass('menu-active')

    // Esconder búsqueda.
    if ($('.sidebar').hasClass('sidebar__active')) {
      $('.sidebar-item--search-trigger').removeClass('hidden')
      $('.sidebar-item--search').addClass('hidden')
    }
  })

  if ($('.slick').length > 0) {
    $('.slick').slick({
      dots: false,
      infinite: false,
      speed: 400,
      arrows: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      mobileFirst: true,
      respondTo: 'min',
      responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 2
          }
        }
      ]
    })
  }

  if ($('.swipe-menu').length > 0) {
    $('.swipe-menu').slick({
      dots: false,
      infinite: false,
      arrows: false,
      mobileFirst: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 769,
          settings: 'unslick'
        }
      ]
    })
  }

  $('.swipe-menu__cell').on('click', function () {
    $('.swipe-menu__cell').removeClass('swipe-menu__cell--active')
    $(this).addClass('swipe-menu__cell--active')
    $('.container-artista .tab, .container-pais .tab').removeClass('tab--active')
    $($(this).data('trigger')).addClass('tab--active')
  })

  $('.btn-artist-follow').on('click', function () {
    $(this).toggleClass('mus-btn--lined')
    if ($(this).text() === 'Seguir') {
      $(this).text('Dejar de seguir')
    } else {
      $(this).text('Seguir')
    }
  })

  // Control de altura en items:
  $('.list-albums__square').matchHeight()

  $('.sidebar-item--search-trigger').on('click', function () {
    $(this).addClass('hidden')
    $('.sidebar-item--search').removeClass('hidden')
    $('.sidebar-item--search input').focus()
  })

}) // End Ready
